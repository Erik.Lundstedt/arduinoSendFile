;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; (defun print (filename message)		;;
;;   (with-sd-card (str filename 2)	;;
;;   (print message str))						;;
;;   )																;;
;; 																	;;
;; (defun list (filename)						;;
;;   (with-sd-card (str filename)		;;
;;     (loop					 							;;
;;      (let ((l (read-line str)))	  ;;
;;        (unless l (return nothing)) ;;
;;        (princ l)				 				;;
;;        (terpri)))))			 				;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



(with-sd-card (str "Greeting.txt" 2)
  (print "Hello" str))


(with-sd-card (str "Greeting.txt")
  (read str))
